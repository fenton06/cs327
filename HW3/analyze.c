
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "analyze.h"
#include "weatherio.h"
#include "util.h"

/*
 * Helper functions
 */

// Parse query - returns 0 if successful
int doQuery(char *dataFilename, char *line, char **out) {

    // Save original line
    char origLine[MAX_LINE_LENGTH];
    strcpy(origLine, line);

    // Query info
    char *queryType;

    // Get query type
    queryType = strtok(line, " ");

    // Check query is a valid query
    if(strcmp(queryType, "histogram") != 0 && strcmp(queryType, "min") != 0 &&
       strcmp(queryType, "max") != 0 && strcmp(queryType, "average") != 0) {
        return -1;
    }

    int result = 0;

    // Call correct query functions
    if(strcmp(queryType, "histogram") == 0) {
        result = histogram(dataFilename, origLine, out);
    } else if(strcmp(queryType, "min") == 0) {
        result = minimum(dataFilename, origLine, out);
    } else if (strcmp(queryType, "max") == 0) {
        result = maximum(dataFilename, origLine, out);
    } else {
        result = average(dataFilename, origLine, out);
    }


    if(result != 0) {
        return -1;
    }

    // Success!!!
    return 0;
}

int parseLine(char *line, char *columnName, int *bins, char *startDateTime, char *stopDateTime) {

    char *iter;

    // Query type
    char queryType[MAX_LINE_LENGTH];
    iter = strtok(line, " ");
    strcpy(queryType, iter);

    // Column name
    iter = strtok(NULL, " ");
    if(strcmp(iter, "Wind") == 0) {
        char temp[MAX_LINE_LENGTH];
        strcpy(temp, iter);
        strcat(temp, " ");
        iter = strtok(NULL, " ");
        strcat(temp, iter);
        strcpy(columnName, temp);
    } else {
        strcpy(columnName, iter);
    }

    if(strcmp(queryType, "histogram") == 0) {
        iter = strtok(NULL, " ");

        if(atoi(iter) != 0) {
            *bins = atoi(iter);
        }
    }

    // Start time
    iter = strtok(NULL, " ");
    strcpy(startDateTime, iter);

    // Stop time
    iter = strtok(NULL, " ");
    strcpy(stopDateTime, iter);

    return 0;
}

int parseTime(char *dateTime, long *date, int *time) {

    //Separate date and time - CSV is in YYYYMMDD format for date, HHMM for time 0001 (1!!) @ 12:01
    char dateStr[MAX_LINE_LENGTH];
    char timeStr[MAX_LINE_LENGTH];

    char *iter;

    // Get date string
    iter = strtok(dateTime, "-");
    if(iter != NULL) {
        strcpy(dateStr, iter);
    } else {
        return -1;
    }

    // Get time string
    iter = strtok(NULL, "-");
    if(iter != NULL) {
        strcpy(timeStr, iter);
    } else {
        return -1;
    }

    // Separate month day and year, then concat to get actual string
    char month[3];
    char day[3];
    char year[5];

    iter = strtok(dateStr, "/");
    if(iter != NULL) {
        strcpy(month, iter);
    } else {
        return -1;
    }

    iter = strtok(NULL, "/");
    if(iter != NULL) {
        strcpy(day, iter);
    } else {
        return -1;
    }

    iter = strtok(NULL, "/");
    if(iter != NULL) {
        strcpy(year, iter);
    } else {
        return -1;
    }

    // Concatenate to format in CSV
    char *dateCat = (char *) calloc((size_t) 9, sizeof(char));

    strcat(dateCat, year);
    strcat(dateCat, month);
    strcat(dateCat, day);

    if(atol(dateCat) == 0) {
        return -1;
    }

    // Convert date to long
    *date = atol(dateCat);

    free(dateCat);

    // Separate Hr and Min
    char hour[3];
    char minute[4];

    iter = strtok(timeStr, ":");
    if(iter != NULL) {
        strcpy(hour, iter);
    } else {
        return -1;
    }

    iter = strtok(NULL, ":");
    if(iter != NULL) {
        strcpy(minute, iter);
    } else {
        return -1;
    }

    // Concatenate into time string
    char *timeCat = (char *) calloc((size_t) 6, sizeof(char));  // 4 chars + \n + null terminator = 6

    strcat(timeCat, hour);
    strcat(timeCat, minute);

    // Convert date to long ->  accepts 0 as a time so no error checking =/
    *time = atoi(timeCat);

    free(timeCat);

    return 0;
}

int getIndex(char *column, char **headers) {

    int i = 0;
    while(headers[i] != NULL) {
        if(strcmp(column, headers[i]) == 0) {
            return i;
        }

        i++;
    }

    // Not found!!!
    return -1;
}


/*
 * MAIN
 */

int main(int argc, char **argv) {

    // Check for max amount of arguments (3 operators + 1 executable name)
    if (argc > 4) {
        printf("Too many command line args!");
        return -1;
    }

    // Declare argument pointers
    char *dataFilename;
    dataFilename = NULL;

    char *inFilename;
    inFilename = NULL;

    char *outFilename;
    outFilename = NULL;

    // Point program vars to correct position of command line args if needed
    // argv[0] is program name, start at 1
    int i;
    for (i = 1; i < argc; i++) {

        char *type;
        type = strtok( argv[i], "=");

        if(strcmp(type, "df") == 0) {
            // data filename
            type = strtok(NULL, "=");
            dataFilename = type;
        } else if(strcmp(type, "if") == 0) {
            // Input filename
            type = strtok(NULL, "=");
            inFilename = type;
        } else if(strcmp(type, "of") == 0) {
            //  Output filename
            type = strtok(NULL, "=");
            outFilename = type;
        } else {
            printf("Invalid argument!");
            return -2;
        }
    }

    // Get data file if not specified in cmd line
    if(dataFilename == NULL) {
        // Must use temp since original dataFile pointer is NULL
        char *temp;
        printf("Please enter a filename: ");
        scanf("%s", temp);
        dataFilename = temp;
    }

    // Check for valid format!
    int valid = validate_format(dataFilename);

    // Error of some sort
    if(valid != 0) {
        return -3;
    }

    // in/outFile pointers, necessary to possibly write to later!
    FILE *outFile;
    FILE *inFile;

    // If there is an outFilename given, open / create it
    if(outFilename != NULL) {
        outFile = fopen(outFilename, "w");
    }

    // If there is an inFilename given, open it
    if(inFilename != NULL) {
        inFile = fopen(inFilename, "r");
    }

    //No input file
    char line[MAX_LINE_LENGTH]; // take care of null terminator...


    if(inFilename == NULL) {
        // No input filename - take input from console
        fgets(line, MAX_LINE_LENGTH, stdin);

        while(strcmp(line, "quit") != 0 && strcmp(line, "quit\n") != 0) {

            char origLine[MAX_LINE_LENGTH];
            strcpy(origLine, line);

            char *out;

            int result = doQuery(dataFilename, line, &out);

            if(result != 0) {
                printf("Query %s failed.\n", origLine);
            } else {
                // If no outfile write to stdout, else to file
                if(outFilename == NULL) {
                    printf("%s", out);
                    printf("\n");
                } else {
                    fprintf(outFile, "%s\n", out);
                }
            }

            free(out);

            // Get next line
            fgets(line, MAX_LINE_LENGTH, stdin);
        }
    } else {
        while(fgets(line, MAX_LINE_LENGTH, inFile)) {

            char origLine[MAX_LINE_LENGTH];;
            strcpy(origLine, line);

            char *out;

            int result = doQuery(dataFilename, line, &out);

            if(result != 0) {
                printf("Query %s failed.", origLine);
            } else {
                // If no outfile write to stdout, else to file
                if(outFilename == NULL) {
                    // print output to stdout
                    printf("%s", out);
                    printf("\n");
                } else {
                    fprintf(outFile, "%s\n", out);
                }
            }

            free(out);
        }
    }

    // Close outFile iff it was created
    if(outFilename != NULL) {
        fclose(outFile);
    }

    // Close inFile file iff it was created
    if(inFilename != NULL) {
        fclose(inFile);
    }

    return 0;
}


/*
 * Public functions
 */

int histogram(char *dataFilename, char *line, char **out) {

    char columnName[64];
    int bins = 0;
    char startDateTime[64];
    char stopDateTime[64];

    // Parse query...
    int parseResult = parseLine(line, columnName, &bins, startDateTime, stopDateTime);

    if(parseResult != 0) {
        return -1;
    }

    long startDate = 0;
    int startTime = 0;
    long stopDate = 0;
    int stopTime = 0;

    // Parse time
    int startTimeResult = parseTime(startDateTime, &startDate, &startTime);
    int stopTimeResult = parseTime(stopDateTime, &stopDate, &stopTime);

    if(startTimeResult != 0 || stopTimeResult != 0) {
        return -1;
    }

    int numHeaders = header_columns(dataFilename);

    char **headers = (char **) calloc((size_t) numHeaders + 1, sizeof(char *));

    read_header(dataFilename, headers);

    void **data = (void **) calloc((size_t) numHeaders + 1, sizeof(void *));

    // Open data file - incorrect data file will be caught when validating...
    FILE *dataFile = fopen(dataFilename, "r");

    // Get and free header row
    char *headerRow = (char *) calloc((size_t) 256, sizeof(char));
    fgets(headerRow, 256, dataFile);
    free(headerRow);

    // Set up type
    Type_t histogramType = column_type(columnName);

    int defaultInt = atoi(get_default(columnName));
    float defaultFloat = atof(get_default(columnName)) / 10;

    void *histoHigh;
    void *histoLow;
    int iHistoHigh = 0;
    int iHistoLow = 999;
    float fHistoHigh = -999.9;
    float fHistoLow = 999.9;

    if(histogramType == INT) {
        histoHigh = &iHistoHigh;
        histoLow = &iHistoLow;
    } else {
        histoHigh = &fHistoHigh;
        histoLow = &fHistoLow;
    }


    int idx = getIndex(columnName, headers);
    int dateIdx = getIndex("Date", headers);
    int timeIdx = getIndex("HrMn", headers);


    // Get high and low values
    while(!feof(dataFile)) {

        int readResult = read_row(dataFile, headers, data);

        if(readResult != 0) {
            return -1;
        }

        long date = *((long*) data[dateIdx]);
        int time = *((int*) data[timeIdx]);

        if((date > startDate && time > startTime) &&
           (date < stopDate && time < stopTime)) {
            if(histogramType == INT) {
                if(*((int*) data[idx]) < *((int*) histoLow) && *((int*) data[idx]) != defaultInt) {
                    *((int*) histoLow) = *((int*) data[idx]);
                } else if(*((int*) data[idx]) > *((int*)histoHigh) && *((int*) data[idx]) != defaultInt) {
                    *((int*) histoHigh) = *((int*) data[idx]);
                }
            } else {
                if(*((float*) data[idx]) < *((float*) histoLow) && *((float*) data[idx]) != defaultFloat) {
                    *((float*) histoLow) = *((float*) data[idx]);
                } else if(*((float*) data[idx]) > *((float*)histoHigh) && *((float*) data[idx]) != defaultFloat) {
                    *((float*) histoHigh) = *((float*) data[idx]);
                }
            }
        }
    }

    // Close file
    fclose(dataFile);

    // Declare range vars
    int iHistoRange = 0;
    float fHistoRange = 0;
    float binWidth = 0;

    // Get range of histogram
    if(histogramType == INT) {
        iHistoRange = *((int *)histoHigh) - *((int *)histoLow);
        binWidth = (float) iHistoRange / (float) bins;
    } else {
        fHistoRange = *((float *)histoHigh) - *((float *)histoLow);
        binWidth = fHistoRange / (float) bins;
    }

    int buckets[bins];

    //Initialize buckets to zeros
    int i = 0;
    for(i = 0; i < bins; i++) {
        buckets[i] = 0;
    }

    // Re-open file to place values in bins
    dataFile = fopen(dataFilename, "r");

    // Get and free header row
    headerRow = (char *) calloc((size_t) 256, sizeof(char));
    fgets(headerRow, 256, dataFile);
    free(headerRow);


    // Read values and determine which bucket to place them into
    while(!feof(dataFile)) {

        int readResult = read_row(dataFile, headers, data);

        if(readResult != 0) {
            return -1;
        }

        long date = *((long*) data[dateIdx]);
        int time = *((int*) data[timeIdx]);

        if((date > startDate && time > startTime) &&
           (date < stopDate && time < stopTime)) {
            if(histogramType == INT) {
                if(*((int*) data[idx]) != defaultInt) {
                    for(i = 0; i < bins; i++) {
                        if(*((int*) data[idx]) < (*((int*) histoLow) + binWidth * (i + 1))) {
                            buckets[i]++;
                            break;
                        }
                    }
                }
            } else {
                if(*((float*) data[idx]) != defaultFloat) {
                    for(i = 0; i < bins; i++) {
                        if(*((float*) data[idx]) < (*((float*) histoLow) + binWidth * (i + 1))) {
                            buckets[i]++;
                            break;
                        }
                    }
                }
            }
        }

        weatherio_cleanup();
    }

    util_cleanup();
    free(headers);
    free(data);

    *out = (char *) calloc((size_t) 512, sizeof(char));

    float lo = *((float*) histoLow);
    float hi = *((float*) histoHigh);

    char output[512];

    sprintf(output, "%s %s\t\t%s\n", columnName, "range", "Occurrences");
    //sprintf(output, "%.4f : %.4f\t%d\n", lo, hi, buckets[0]);

    for(i = 0; i < bins; i++) {
        char temp[32];
        sprintf(temp, "%-6.2f : %-6.2f\t\t%-d\n", lo, hi, buckets[i]);
        strcat(output, temp);

        lo += binWidth;
        hi += binWidth;
    }

    strcpy(*out, output);

    fclose(dataFile);

    return 0;
}

int minimum(char *dataFilename, char *line, char **out){

    char columnName[64];
    int bins = 0;
    char startDateTime[64];
    char stopDateTime[64];

    // Parse query...
    int parseResult = parseLine(line, columnName, &bins, startDateTime, stopDateTime);

    if(parseResult != 0) {
        return -1;
    }

    long startDate = 0;
    int startTime = 0;
    long stopDate = 0;
    int stopTime = 0;

    // Parse time
    int startTimeResult = parseTime(startDateTime, &startDate, &startTime);
    int stopTimeResult = parseTime(stopDateTime, &stopDate, &stopTime);

    if(startTimeResult != 0 || stopTimeResult != 0) {
        return -1;
    }

    int numHeaders = header_columns(dataFilename);

    char **headers = (char **) calloc((size_t) numHeaders + 1, sizeof(char *));

    read_header(dataFilename, headers);

    void **data = (void **) calloc((size_t) numHeaders + 1, sizeof(void *));

    // Open data file - incorrect data file will be caught when validating...
    FILE *dataFile = fopen(dataFilename, "r");

    // Get and free header row
    char *headerRow = (char *) calloc((size_t) 256, sizeof(char));
    fgets(headerRow, 256, dataFile);
    free(headerRow);

    // Set up minimum
    Type_t minType = column_type(columnName);

    void *min;
    int iMin = 9999;
    float fMin = 99999;

    if(minType == INT) {
        min = &iMin;
    } else {
        min = &fMin;
    }

    int idx = getIndex(columnName, headers);
    int dateIdx = getIndex("Date", headers);
    int timeIdx = getIndex("HrMn", headers);

    while(!feof(dataFile)) {

        int readResult = read_row(dataFile, headers, data);

        if(readResult != 0) {
            return -1;
        }

        long date = *((long*) data[dateIdx]);
        int time = *((int*) data[timeIdx]);

        if((date > startDate && time > startTime) &&
           (date < stopDate && time < stopTime)) {

            if(minType == INT) {
                if(*((int*) data[idx]) < *((int*) min)) {
                    *((int*) min) = *((int*) data[idx]);
                }
            } else {
                if(*((float*) data[idx]) < *((float*) min)) {
                    *((float*) min) = *((float*) data[idx]);
                }
            }
        }
        weatherio_cleanup();
    }

    util_cleanup();
    free(headers);
    free(data);

    *out = (char *) calloc((size_t) MAX_LINE_LENGTH, sizeof(char));

    if(minType == INT) {
        sprintf(*out, "Minimum: %d", *((int*) min));
        //strcpy(*out,"Minimum: ");
        //printf("Minimum: %d\n", *((int*) min));
    } else {
        sprintf(*out, "Minimum: %.4f", *((float*) min));
        //printf("Minimum: %.4f\n", *((float*) min));
    }

    fclose(dataFile);

    return 0;
}

int maximum(char *dataFilename, char *line, char **out){

    char columnName[64];
    int bins = 0;
    char startDateTime[64];
    char stopDateTime[64];

    // Parse query...
    int parseResult = parseLine(line, columnName, &bins, startDateTime, stopDateTime);

    if(parseResult != 0) {
        return -1;
    }

    long startDate = 0;
    int startTime = 0;
    long stopDate = 0;
    int stopTime = 0;

    // Parse time
    int startTimeResult = parseTime(startDateTime, &startDate, &startTime);
    int stopTimeResult = parseTime(stopDateTime, &stopDate, &stopTime);

    if(startTimeResult != 0 || stopTimeResult != 0) {
        return -1;
    }

    int numHeaders = header_columns(dataFilename);

    char **headers = (char **) calloc((size_t) numHeaders + 1, sizeof(char *));

    read_header(dataFilename, headers);

    void **data = (void **) calloc((size_t) numHeaders + 1, sizeof(void *));

    // Open data file - incorrect data file will be caught when validating...
    FILE *dataFile = fopen(dataFilename, "r");

    // Get and free header row
    char *headerRow = (char *) calloc((size_t) 256, sizeof(char));
    fgets(headerRow, 256, dataFile);
    free(headerRow);

    // Set up type
    Type_t maxType = column_type(columnName);

    void *max;
    int iMax = 0;
    float fMax = -9999;

    if(maxType == INT) {
        max = &iMax;
    } else {
        max = &fMax;
    }

    int defaultInt = atoi(get_default(columnName));
    float defaultFloat = atof(get_default(columnName)) / 10;

    int idx = getIndex(columnName, headers);
    int dateIdx = getIndex("Date", headers);
    int timeIdx = getIndex("HrMn", headers);

    while(!feof(dataFile)) {

        int readResult = read_row(dataFile, headers, data);

        if(readResult != 0) {
            return -1;
        }

        long date = *((long*) data[dateIdx]);
        int time = *((int*) data[timeIdx]);

        if((date > startDate && time > startTime) &&
           (date < stopDate && time < stopTime)) {

            if(maxType == INT) {
                if(*((int*) data[idx]) > *((int*) max) && *((int*) data[idx]) != defaultInt) {
                    *((int*) max) = *((int*) data[idx]);
                }
            } else {
                if(*((float*) data[idx]) > *((float*) max) && *((float*) data[idx]) != defaultFloat) {
                    *((float*) max) = *((float*) data[idx]);
                }
            }
        }
        weatherio_cleanup();
    }

    util_cleanup();
    free(headers);
    free(data);

    *out = (char *) calloc((size_t) MAX_LINE_LENGTH, sizeof(char));

    if(maxType == INT) {
        sprintf(*out, "Maximum: %d", *((int*) max));
    } else {
        sprintf(*out, "Maximum: %.4f", *((float*) max));
    }

    fclose(dataFile);

    return 0;
}

int average(char *dataFilename, char *line, char **out){

    char columnName[64];
    int bins = 0;
    char startDateTime[64];
    char stopDateTime[64];

    // Parse query...
    int parseResult = parseLine(line, columnName, &bins, startDateTime, stopDateTime);

    if(parseResult != 0) {
        return -1;
    }

    long startDate = 0;
    int startTime = 0;
    long stopDate = 0;
    int stopTime = 0;

    // Parse time
    int startTimeResult = parseTime(startDateTime, &startDate, &startTime);
    int stopTimeResult = parseTime(stopDateTime, &stopDate, &stopTime);

    if(startTimeResult != 0 || stopTimeResult != 0) {
        return -1;
    }

    int numHeaders = header_columns(dataFilename);

    char **headers = (char **) calloc((size_t) numHeaders + 1, sizeof(char *));

    read_header(dataFilename, headers);

    void **data = (void **) calloc((size_t) numHeaders + 1, sizeof(void *));

    // Open data file - incorrect data file will be caught when validating...
    FILE *dataFile = fopen(dataFilename, "r");

    // Get and free header row
    char *headerRow = (char *) calloc((size_t) 256, sizeof(char));
    fgets(headerRow, 256, dataFile);
    free(headerRow);

    // Set up type
    Type_t averageType = column_type(columnName);

    void *average;
    int iAverage = 0;
    float fAverage = 0;

    if(averageType == INT) {
        average = &iAverage;
    } else {
        average = &fAverage;
    }

    int defaultInt = atoi(get_default(columnName));
    float defaultFloat = atof(get_default(columnName)) / 10;

    int idx = getIndex(columnName, headers);
    int dateIdx = getIndex("Date", headers);
    int timeIdx = getIndex("HrMn", headers);

    int numEntries = 0;

    while(!feof(dataFile)) {

        int readResult = read_row(dataFile, headers, data);

        if(readResult != 0) {
            return -1;
        }

        long date = *((long*) data[dateIdx]);
        int time = *((int*) data[timeIdx]);

        if((date > startDate && time > startTime) &&
           (date < stopDate && time < stopTime)) {

            numEntries++;

            if(averageType == INT) {
                if(*((int*) data[idx]) != defaultInt) {
                    *((int*) average) += *((int*) data[idx]);
                }
            } else {
                if(*((float*) data[idx]) != defaultFloat){
                    *((float*) average) += *((float*) data[idx]);
                }
            }
        }
        weatherio_cleanup();
    }

    // Get average
    *((int*) average) = *((int*) average)/numEntries;

    util_cleanup();
    free(headers);
    free(data);

    *out = (char *) calloc((size_t) MAX_LINE_LENGTH, sizeof(char));

    if(averageType == INT) {
        sprintf(*out, "Average: %d", *((int*) average));
    } else {
        sprintf(*out, "Average: %.4f", *((float*) average));
    }

    fclose(dataFile);

    return 0;
}
