//
// Created by Benjamin on 10/20/2015.
//

#ifndef HW3_ANALYZE_H

// 80 bytes + null terminator
#define MAX_LINE_LENGTH 81
#define DATE_LENGTH 11
#define TIME_LENGTH 7 // 5 char + newline + null terminator

/*
 * Create a histogram (table) of values that represents a histogram of the data.
 *
 * histogram <header> <bins> <start date/time> <end date/time>
 *
 * Bins is an integer that represents the number of equally spaced bins
 * to use in the histogram between the minimum and the maximum element
 * of data in the data set for the column named in header.
 */
int histogram(char *, char *, char **);

/*
 * Compute minimum value in column over time range, as named by header.
 * Valid for numerical columns only.
 *
 * min <header> <start date/time> <end date/time>
 */

int minimum(char *, char *, char **);

/*
 * Compute maximum value in column over time range, as named by header.
 * Valid for numerical columns only.
 *
 * max <header> <start date/time> <end date/time>
 */

int maximum(char *, char *, char **);

/*
 * Compute average value in column over time range, as named by header.
 * Valid for numerical columns only.
 *
 * average <header> <start date/time> <end date/time>
 */

int average(char *, char *, char **);

#define HW3_ANALYZE_H

#endif //HW3_ANALYZE_H
