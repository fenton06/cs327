//
// Created by Benjamin on 12/6/2015.
//

#include "Util.h"

namespace util {

    std::string trim(std::string &str) {

        // Check for 0 length!
        if(str.length() == 0) {
            return str;
        }

        // Initialize indices
        int front = 0;
        int back = (int) str.length() - 1;

        // Find whitespace at front
        while(isspace(str[front])) {
            front++;
        }

        // Find whitespace at back
        while(isspace(str[back])) {
            back--;
        }

        return str.substr((unsigned int) front, (unsigned int) back - front + 1);
    }

    std::string toUpper(std::string &str) {

        for(int i = 0; i < str.length(); i++) {
            str[i] = (char) toupper(str[i]);
        }

        return str;
    }

    std::string wordToken(std::string &str) {

        // Check for 0 length!
        if(str.length() == 0) {
            return str;
        }

        // Initialize first indice
        int first = 0;

        // Find first non-whitespace character
        while(!isalnum(str[first])) {
            first++;
        }

        // Initialize last indice
        int last = first;

        // Find last character
        while(isalnum(str[last])) {
            last++;
        }

        std::string token = str.substr((unsigned int) first, (unsigned int) last);

        // Remove token
        str.erase(0, (unsigned int) last);

        return token;
    }

    std::string numberToken(std::string &str) {

        // Check for 0 length!
        if(str.length() == 0) {
            return str;
        }

        // Initialize first indice
        int first = 0;

        // Find first non-whitespace character
        while(!isdigit(str[first]) && str[first] != '-') {
            first++;
        }

        // Initialize last indice
        int last = first + 1;

        // Find last character
        while(isdigit(str[last])) {
            last++;
        }

        std::string token = str.substr((unsigned int) first, (unsigned int) last);

        // Remove token
        str.erase(0, (unsigned int) last);

        return token;
    }



}

