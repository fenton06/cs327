//
// Created by Benjamin on 12/3/2015.
//

#define PI 3.1415926535897

#include "WaveGenerator.h"
#include <math.h>

using namespace std;

// Constructor
WaveGenerator::WaveGenerator() {

    // Wave info
    waveType = ""; // sine, triangle, sawtooth, pulse
    pulseUp = 0;

    sampleRate = 0;  // Any value
    bitRes = 0;  // Must be 8, 16, or 32!!!
    frequency = 0;
    length = 0;
    peakVolume = -1; // 0 <= volume <= 1  (percentage)

    // ADSR vars
    attack = 0;
    decay = 0;
    sustain = -1; // 0 <= volume <= 1  (percentage)
    release = 0;

    // Calculated - not required
    numSamples = (int) (sampleRate * length);

}

// Generates ADSR sound
bool WaveGenerator::createADSRSound(SoundFile &waveSnd) {

    if(!checkWaveProps() || !checkADSRProps()) {
        return false;
    }

    waveSnd.sampleRate = this->sampleRate;
    waveSnd.bitRes = this->bitRes;
    waveSnd.numChannels = 1; // Single channel produced
    waveSnd.length = this->length;
    waveSnd.numSamples = this->numSamples;

    vector<double> sampleVals = quantizeWave();

    // Remove last element (not quantized)
    sampleVals.pop_back();

    // Apply ADSR envelope
    applyADSR(sampleVals);

    this-> channelInfo = getChannelVals(sampleVals);

    return true;
}

/*
 * Helper functions (private)
 */


// Checks to make sure all required variables have values
bool WaveGenerator::checkWaveProps() {

    // Check wave types are set
    if(waveType.compare("") == 0) {
        cerr << "No wave type specified!" << endl;
        return 0;
    } else if(waveType.compare("pulse") == 0 && pulseUp <= 0) {
        cerr << "No pulse uptime specified!" << endl;
        return 0;
    }

    // Check wave params
    if(sampleRate == 0) {
        cerr << "SampleRate undefined!!!" << endl;
        return false;
    }

    if(bitRes == 0) {
        cerr << "Bit depth undefined!!!" << endl;
        return false;
    }

    if(frequency == 0) {
        cerr << "Frequency undefined!!!" << endl;
        return false;
    }

    if(length == 0) {
        cerr << "Duration undefined!!!" << endl;
        return false;
    }

    if(peakVolume == -1) {
        cerr << "Peak volume undefined!!!" << endl;
        return false;
    }

    return true;
}

// Check ADSR envelope properties
bool WaveGenerator::checkADSRProps() {

    // Check ADSR props
    if(attack == 0) {
        cerr << "ADSR attack duration undefined!!!" << endl;
        return false;
    }

    if(decay == 0) {
        cerr << "ADSR decay duration undefined!!!" << endl;
        return false;
    }

    if(sustain == -1) {
        cerr << "ADSR sustain volume undefined!!!" << endl;
        return false;
    }

    if(release == 0) {
        cerr << "ADSR release undefined!!!" << endl;
        return false;
    } else if (release >= length) {
        cerr << "ADSR release longer than sample!!!" << endl;
        return false;
    }

    return true;
}

// Quantize generated wave data
vector<double> WaveGenerator::quantizeWave() {

    vector<double> sampleVals;

    if(waveType.compare("sine") == 0) {
        sampleVals = sampleSine();
    } else if(waveType.compare("triangle") == 0) {
        sampleVals = sampleTriangle();
    } else if(waveType.compare("sawtooth") == 0) {
        sampleVals = sampleSawtooth();
    } else { // Must be pulse...
        sampleVals = samplePulse();
    }

    // Quantize (sampleVals (0, 1) -> split difference, (1,2) -> split difference, etc
    for(unsigned i = 0; i < numSamples; i++) {

        double quant = (sampleVals.at(i) + sampleVals.at(i+1)) / 2;

        // Apply volume percentage...
        quant  = quant * peakVolume;

        sampleVals.at(i) = quant;
    }

    return sampleVals;
}

// Create sine wave values
vector<double> WaveGenerator::sampleSine() {

    // How often to take samples
    double interval = 1.0 / sampleRate;

    int amplitude = (int) pow(2, bitRes - 1) - 1;

    // Vector containing values at each sample time
    vector<double> sampleVals;

    double sampleTime = 0;

    for(int i = 0; i <= numSamples; i++) {

        double val = amplitude * sin(frequency * sampleTime);

        sampleVals.push_back(val);

        sampleTime += interval;
    }

    return sampleVals;
}

// Create triangle wave values
vector<double> WaveGenerator::sampleTriangle() {

    // How often to take samples
    double interval = 1.0 / sampleRate;

    int amplitude = (int) pow(2, bitRes - 1) - 1;

    // Vector containing values at each sample time
    vector<double> sampleVals;

    double sampleTime = 0;

    for(int i = 0; i <= numSamples; i++) {

        double val = ((2 * amplitude) / PI ) * asin(sin(2 * PI * frequency * sampleTime));

        sampleVals.push_back(val);

        sampleTime += interval;

    }

    return sampleVals;
}

// Create sawtooth wave values
vector<double> WaveGenerator::sampleSawtooth() {

    // How often to take samples
    double interval = 1.0 / sampleRate;

    int amplitude = (int) pow(2, bitRes - 1) - 1;

    // Vector containing values at each sample time
    vector<double> sampleVals;

    double sampleTime = 0;

    for(int i = 0; i <= numSamples; i++) {

        double val = 0.0;

        if(sampleTime != 0 ) {
            val = ((-2 * amplitude) / PI) * atan(1/tan(PI * frequency * sampleTime));
        }

        sampleVals.push_back(val);

        sampleTime += interval;

    }

    return sampleVals;
}

// Create pulse (square) wave values
vector<double> WaveGenerator::samplePulse() {

    // How often to take samples
    double interval = 1.0 / sampleRate;

    int amplitude = (int) pow(2, bitRes - 1) - 1;

    // Vector containing values at each sample time
    vector<double> sampleVals;

    double sampleTime = 0;

    for(int i = 0; i <= numSamples; i++) {

        if(sampleTime >= 0 && sampleTime < (pulseUp/frequency)) {
             sampleVals.push_back((double) amplitude);
        } else if(sampleTime >= (pulseUp/frequency) && sampleTime < (1.0/frequency)) {
            sampleVals.push_back((double) (-1 * amplitude));
        } else { // SampleTime is equal to period!
            sampleTime = 0;
            sampleVals.push_back((double) amplitude);
        }
        sampleTime += interval;
    }

    return sampleVals;
}

//  Applies ADSR envelope volume profile
void WaveGenerator::applyADSR(vector<double> &sampleVals) {

    // How often to take samples
    double interval = 1.0 / sampleRate;

    double volPercent;

    double sampleTime = 0.0;
    double decayTime = 0.0;
    double releaseTime = 0.0;

    double attackDur = attack;
    double decayDur = decay;

    double endAttackVol = 1.0;
    double endDecayVol = sustain;

    bool maxAttack = true;

    if(attack + release >= length) {

        decayDur = 0.0;
        attackDur = length - release;
        maxAttack = false;

    } else if(attack + decay + release >= length) {

        decayDur = length - (attack + release);

    }

    for(unsigned i = 0; i < sampleVals.size(); i++) {

        double val = sampleVals.at(i);

        // Attack
        if(sampleTime <= attackDur) {

            volPercent = (1 / attack) * sampleTime;

            val *= volPercent;
            sampleVals.at(i) = val;

            endAttackVol = volPercent;
        }

        // Decay
        if(sampleTime > attackDur && sampleTime <= (attackDur + decayDur)) { // Decay section

            volPercent = ((sustain - endAttackVol) / decay) * decayTime + endAttackVol;

            val *= volPercent;
            sampleVals.at(i) = val;

            decayTime += interval;

            endDecayVol = volPercent;
        }

        // Sustain
        if(sampleTime > (attack + decay) && sampleTime <= (length - release)) { // Sustain

            val *= sustain;
            sampleVals.at(i) = val;

        }

        // Release
        if(sampleTime > length - release){ // Release

            if(maxAttack) {
                volPercent = -1 * (endDecayVol / release) * releaseTime + endDecayVol;
            } else {
                volPercent = -1 * (endAttackVol / release) * releaseTime + endAttackVol;
            }



            val *= volPercent;
            sampleVals.at(i) = val;

            releaseTime += interval;
        }

        sampleTime += interval;
    }

    return;
}

// Creates vector for channelInfo object variable
vector< vector<int> > WaveGenerator::getChannelVals(vector<double> &sampleVals) {

    vector< vector<int> > channelVals;

    // Round vectors to nearest whole number and convert to int
    for(unsigned i = 0; i < numSamples; i++) {

        int channelVal = (int) round(sampleVals.at(i));

        // Create int vector to place value
        vector<int> valVec;
        valVec.push_back(channelVal);

        channelVals.push_back(valVec);
    }

    return channelVals;
}