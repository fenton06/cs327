//
// Created by Benjamin on 12/3/2015.
//

#include "sndcat.h"
//#include "SoundFile.h"
//#include <iostream>
//#include <string>
//#include <vector>

using namespace std;

int main(int argc, char *argv[]) {

    string outFilename = "";

    // Check for switches
    for(int i = 1; i < argc; i++) {
        string arg = argv[i];

        // Display help and terminate
        if(arg.compare("-h") == 0) {

            string help = "\nUsage: sndcat [switches] file1 file2 ... filen\n"
                    "\n"
                    "Switches:\n"
                    "\n"
                    "\t-h\t\tSpecify the output file (if omitted, writes to stdout)\n"
                    "\t-o\t\tSpecify output file\n"
                    "\t\n"
                    "Reads all sound files passed as arguments, and writes a single sound file that isthe concatenation of the inputs. If no files are passed as arguments, then the program should read from standard input. If no output file is specified it writes to stdout.";

            cout << help <<endl;

            return 0;

        } else if(arg.compare("-o") == 0) {
            i++;
            outFilename = argv[i];
        }
    }

    int numFiles = 0;

    // Set number of files passed
    if(outFilename.compare("") == 0) {
        numFiles = argc - 1;
    } else {
        numFiles = argc - 3;
    }

    // Create vector of sound files and input filename string
    vector<SoundFile> soundVec;
    string inFilename;

    // No filenames passed, parse filenames from stdin
    if(numFiles == 0) {
        while(getline(cin, inFilename)) {

            SoundFile sound(inFilename);

            // Check if SoundFile was successfully read -> push to vector
            if(sound.readCS229SoundFile()) {
                soundVec.push_back(sound);
            } else {
                cerr << "Invalid CS229 file: " << inFilename << endl;
                return 0;
            }
        }
    } else {

        // Parse passed filenames
        for(int i = argc - numFiles; i < argc; i++) {

            inFilename = argv[i];
            SoundFile sound(inFilename);

            // Check if SoundFile was successfully read -> push to vector
            if(sound.readCS229SoundFile()) {
                soundVec.push_back(sound);
            } else {
                cerr << "Invalid CS229 file: " << inFilename << endl;
                return 0;
            }
        }
    }

    // Check if there was a single filename (no concat needed...)
    if(soundVec.size() == 1) {
        // No output file specified
        if(outFilename.compare("") == 0) {
            (soundVec.at(0)).printCSS229Sound();
        } else {
            (soundVec.at(0)).writeCS229SoundFile(outFilename);
        }
        return 0;
    }

    if(sndcat::checkConcatSoundProps(soundVec)) {
        // Concatenate sounds -> channel info is added to first SoundFile object
        sndcat::concatSounds(soundVec);

        // No output file specified
        if(outFilename.compare("") == 0) {
            (soundVec.at(0)).printCSS229Sound();
        } else {
            (soundVec.at(0)).writeCS229SoundFile(outFilename);
            cout << "Sound files concatenated successfilly!" << endl;
        }
    } else {
        cerr << "File properties not compatible!!!" << endl;
        cout << "Files not concatenated." << endl;
    }

    return 0;
}

namespace  sndcat {

    bool checkConcatSoundProps(std::vector<SoundFile> soundVec) {

        // Set initial param vals to first SoundFile vals to check for concat compatibility
        int catNumChannels = soundVec.at(0).numChannels;
        int catSampleRate = soundVec.at(0).sampleRate;
        int catBitRes = soundVec.at(0).bitRes;

        for(unsigned i = 1; i < soundVec.size(); i++) {
            // Check if values all match!!!
            if(soundVec.at(i).numChannels != catNumChannels) {
                std::cerr << "Channels in sound file " << i << " does not match the rest!" << std::endl;
                return false;
            }

            if(soundVec.at(i).sampleRate != catSampleRate) {
                std::cerr << "SampleRate in sound file " << i << " does not match the rest!" << std::endl;
                return false;
            }

            if(soundVec.at(i).bitRes != catBitRes) {
                std::cerr << "BitRes in sound file " << i << " does not match the rest!" << std::endl;
                return false;
            }
        }

        return true;
    }

    void concatSounds(vector<SoundFile> &soundVec) {

        for(unsigned i = 1; i < soundVec.size(); i++) {

            // Append channel info vector to first SoundFile channel info
            for(unsigned j = 0; j < soundVec.at(i).channelInfo.size(); j++) {
                soundVec.at(0).channelInfo.push_back(soundVec.at(i).channelInfo.at(j));
                soundVec.at(0).numSamples++;
            }
        }

        return;
    }
}
