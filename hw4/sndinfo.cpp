//
// Created by Benjamin on 12/3/2015.
//

#include "sndinfo.h"
//#include "SoundFile.h"
//#include <iostream>
#include <string.h>

using namespace std;

int main(int argc, char *argv[]) {

    //  Following UNIX switch convention, -h MUST be second argument!!!
    if(argc != 1 && !(strcmp(argv[1], "-h"))) {

        string help = "\nUsage: sndinfo [switches] file1 file2 ... filen\n"
                "\n"
                "Switches:\n"
                "\n"
                "\t-h\t\tDisplays help\n"
                "\n"
                "Reads all sound files passed as arguments, and displays the information for each file. If no files are passed as arguments, then the program reads from standard input.";

        cout << help <<endl;

        return 0;
    }

    string filename;
    int i = 1;

    // If no filenames given, take input from stdin
    if(argc == 1) {
        while(getline(cin, filename)) {
            sndinfo::readSound(filename, i);
        }
    } else {

        // Read input from given filenames
        for(i = 1; i < argc; i++) {
            filename = argv[i];
            sndinfo::readSound(filename, i);
        }
    }

    return 0;
}


namespace sndinfo {

    void readSound(string inFilename, int inI) {

        SoundFile sound(inFilename);

        // Read sound from filename
        if(sound.readCS229SoundFile()) {
            cout << "\nFile "<< inI << ":\n" << endl;
            printInfo(sound);
        } else {
            cerr << "File "<< inI << " (" << inFilename <<  ") is invalid! " << endl;
        }

        return;
    }

    void printInfo(SoundFile sound) {

        cout << "Filename: " << sound.filename << endl;
        cout << "Type: " << sound.fileType << endl;
        cout << "Sample Rate: " << sound.sampleRate << endl;
        cout << "Bit Resolution: " << sound.bitRes << endl;
        cout << "Channels: " << sound.numChannels << endl;
        cout << "Total Samples: " << sound.numSamples <<  endl;
        cout << "Length (s): " << sound.length << "\n" << endl;

        return;
    }
}
