//
// Created by Benjamin on 12/3/2015.
//

#ifndef HW4_SNDINFO_H
#define HW4_SNDINFO_H

#include "SoundFile.h"


namespace sndinfo{

    // Reads and stores all the sound information
    void readSound(std::string inFilename, int inI);

    // Prints sound info to stdout
    void printInfo(SoundFile sound);
}

#endif //HW4_SNDINFO_H
