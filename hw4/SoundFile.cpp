//
// Created by Benjamin on 12/3/2015.
//

#include "SoundFile.h"
#include "Util.h"


using namespace std;

// Constructor
SoundFile::SoundFile(string passedFilename) {

    filename = passedFilename;
    fileType = "CS229";

    // Initialize to zero!!
    sampleRate = 0;  // Any value
    bitRes = 0;  // Must be 8, 16, or 32!!!
    numChannels = 0;  // Must be less than 128 (and greater than zero!)
    length = 0;

    // Optional parameter
    numSamples = 0;
}

// Read .CS229 file and place info in sound object
bool SoundFile::readCS229SoundFile() {

    //Open file
    ifstream inFile(filename);

    // Check if file opened (valid filename)
    if(!inFile.is_open()) {
        cerr << "Could not open file: " << filename << endl;
        return false;
    }

    // Parse file into vector
    vector<string> fileVec;

    if(!parseFile(inFile, fileVec)) {
        return false;
    }

    if(!parseVector(fileVec)) {
        return false;
    }

    // Determine length (number of samples / sample rate)
    length = (double) numSamples / (double) sampleRate;

    return true;
}

// Write .CS229 file
bool SoundFile::writeCS229SoundFile(string filename) {

    // Open file
    ofstream outFile(filename);

    // Check if file opened (valid filename)
    if(!outFile.is_open()) {
        cerr << "Could not open file: " << filename << endl;
        return false;
    }

    outFile << "CS229\n" << endl;
    outFile << "# Header Info\n" << endl;
    outFile << "SampleRate\t" << sampleRate << endl;
    outFile << "BitRes\t\t" << bitRes << endl;
    outFile << "Channels\t" << numChannels << endl;
    outFile << "Samples\t\t" << numSamples << endl;
    outFile << "\nStartData\n" << endl;

    // Print channel info
    for(int i = 0; i < numSamples; i++) {
        for(int j = 0; j < numChannels; j++) {
            outFile << channelInfo[i][j] << '\t';
        }
        outFile << endl;
    }

    return true;
}

//Output to stdout
void SoundFile::printCSS229Sound() {

    cout << "CS229\n" << endl;
    cout << "# Header Info\n" << endl;
    cout << "SampleRate\t" << sampleRate << endl;
    cout << "BitRes\t\t" << bitRes << endl;
    cout << "Channels\t" << numChannels << endl;
    cout << "Samples\t\t" << numSamples << endl;
    cout << "\nStartData\n" << endl;

    // Print channel info
    for(unsigned i = 0; i < numSamples; i++) {
        for(unsigned j = 0; j < numChannels; j++) {
            cout << channelInfo.at(i).at(j) << '\t';
        }
        cout << "\n";
    }

    return;
}

/*
 * Helper functions (private)
 */

// Parse file into vector
bool SoundFile::parseFile(ifstream &inFile, vector<string> &fileVec) {

    string line;

    getline(inFile, line);
    line = util::toUpper(line);

    // Check for first line being cs229 or variant
    if(line.compare("CS229")) {
        cerr << "First line of file not a variant of cs229!" << endl;
        return false;
    }

    // Read file into vector for processing
    while(getline(inFile, line)) {
        // Only place lines in vector that are not blank or comments
        if(line.compare("")) {

            // Trim whitespace if not a blank line
            line = util::trim(line);

            if(line.at(0) != '#') {

                // Convert to uppercase
                line = util::toUpper(line);

                // Push to end of vector
                fileVec.push_back(line);
            }
        }
    }

    return true;
}

bool SoundFile::parseVector(std::vector<std::string> &fileVec) {

    // Read vector into object info
    string line;

    // Get header info
    int i = 0;
    for(i = 0; i < fileVec.size(); i++) {

        line = fileVec[i];
        string token = util::wordToken(line);

        if(token.compare("SAMPLERATE") == 0) {

            try {
                token = util::numberToken(line);

                // Check for decimal point (doubles!)
                if(token.find('.') != -1) {
                    cerr << "SampleRate value invalid!! (double)" << endl;
                    return false;
                }

                sampleRate = stoi(token);

                if(sampleRate <= 0) {
                    cerr << "SampleRate value invalid!!" << endl;
                    return false;
                }

            } catch (...) {
                cerr << "Could not convert SampleRate value to int." << endl;
                return false;
            }

            token = util::wordToken(line);
            if(token.compare("") != 0) {
                cerr << "Extra data in SampleRate header!!!" << endl;
                return false;
            }

        } else if(token.compare("BITRES") == 0) {

            try {
                token = util::numberToken(line);

                if(token.find('.') != -1) {
                    cerr << "BitRes value invalid!!! (double)" << endl;
                    return false;
                }

                bitRes = stoi(token);

                if(bitRes <= 0) {
                    cerr << "BitRes value invalid!!" << endl;
                    return false;
                }

            } catch (...) {
                cerr << "Could not convert BitRes value to int." << endl;
                return false;
            }

            token = util::wordToken(line);
            if(token.compare("") != 0) {
                cerr << "Extra data in BitRes header!!!" << endl;
                return false;
            }

        } else if(token.compare("CHANNELS") == 0) {

            try {
                token = util::numberToken(line);

                if(token.find('.') != -1) {
                    cerr << "Channels value invalid!!! (double)" << endl;
                    return false;
                }

                numChannels = stoi(token);

                if(numChannels <= 0) {
                    cerr << "Number of channels invalid!!" << endl;
                    return false;
                }

            } catch (...) {
                cerr << "Could not convert Channels value to int." << endl;
                return false;
            }

            token = util::wordToken(line);
            if(token.compare("") != 0) {
                cerr << "Extra data in Channels header!!!" << endl;
                return false;
            }

        } else if(token.compare("SAMPLES") == 0) {

            try {
                token = util::numberToken(line);

                if(token.find('.') != -1) {
                    cerr << "Samples value invalid!!! (double)" << endl;
                    return false;
                }

                numSamples = stoi(token);

                if(numSamples <= 0) {
                    cerr << "Number of samples invalid!!" << endl;
                    return false;
                }

            } catch (...) {
                cerr << "Could not convert Samples value to int." << endl;
                return false;
            }

            token = util::wordToken(line);
            if(token.compare("") != 0) {
                cerr << "Extra data in Samples header!!!" << endl;
                return false;
            }

        } else if(token.compare("STARTDATA") == 0) {
            break;
        } else {
            cerr << "Invalid header: " << line << endl;
            return false;
        }
    }

    // Check required info is set and valid
    if(sampleRate <= 0) {
        cerr << "Sample rate not specified!" << endl;
        return false;
    }

    if(bitRes == 0) {
        cerr << "Bit resolution not specified!" << endl;
        return false;
    }

     if(bitRes != 8 && bitRes != 16 && bitRes != 32) {
        cerr << "Invalid bit resolution specified!" << endl;
        return false;
    }

    if(numChannels == 0) {
        cerr << "Number of channels not specified!" << endl;
        return false;
    }

    if(numChannels > 127 || numChannels < 1) {
        cerr << "Invalid number of channels specified!" << endl;
        return false;
    }

    // Increment i to get past STARTDATA
    i++;

    int lineNum = 1;
    //Read channel info into array
    for(i; i < fileVec.size(); i++) {

        line = fileVec[i];

        vector<int> row;

        string sampleStr;

        for(int j = 0; j < numChannels; j++) {

            sampleStr = util::numberToken(line);

            if(sampleStr.compare("") == 0) {
                cerr << "Missing channel value in sample line " << lineNum << "!!!" << endl;
                return false;
            }

            // Check for doubles...
            if(sampleStr.find('.') != -1) {
                cerr << "Channel value invalid!!! (double)" << endl;
                return false;
            }

            try {

                int sampleVal = stoi(sampleStr);

                // Check that values are valid for bit resolution!
                switch(bitRes) {

                    case 8  :
                        if(sampleVal < -127 || sampleVal > 127) {
                            cerr << "Sample value out of range in sample line " << lineNum << "!!!" << endl;
                            return false;
                        }
                    case 16 :
                        if(sampleVal < -32767 || sampleVal > 32767) {
                            cerr << "Sample value out of range in sample line " << lineNum << "!!!" << endl;
                            return false;
                        }
                    case 32 :
                        if(sampleVal < -2147483647 || sampleVal > 2147483647) {
                            cerr << "Sample value out of range in sample line " << lineNum << "!!!" << endl;
                            return false;
                        }

                    // No default case handling needed since bit resolution values have already been checked!

                }

                row.push_back(sampleVal);

            } catch (...) {
                cerr << "Could not convert sample value to int." << endl;
                return false;
            }
        }

        sampleStr = util::wordToken(line);
        if(sampleStr.compare("") != 0) {
            cerr << "Extra data in sample line " << lineNum << "!!!" << endl;
            return false;
        }

        channelInfo.push_back(row);
        lineNum++;
    }

    // Check if samples was specified, set value if not
    if(numSamples == 0) {
        numSamples = (int) channelInfo.size();
    } else if (numSamples != channelInfo.size()) {
        cerr << "File does not contain number of samples specified!" << endl;
        return false;
    }

    // Successful parse!!!
    return true;
}