//
// Created by Benjamin on 12/3/2015.
//

#ifndef HW4_SNDMIX_H
#define HW4_SNDMIX_H

#include "SoundFile.h"

namespace sndmix {

    /*
    * Checks properties of each sound file to make sure they are compatible for concatenation.
    *
    * For simplicity, numChannels, sampleRate, and bitRes must be the same.
    *
    */
    bool checkMixSoundProps(std::vector<SoundFile> soundVec);

    // Multiplies channelInfo in each vector to optain correct volume levels
    bool multSounds(std::vector<double> &multVec, std::vector<SoundFile> &soundVec);

    // Adds the channelInfo from each sound to the first sound in soundVec
    void mixSounds(std::vector<SoundFile> &soundVec);

    // Clips sounds to be within the limits of bitRes
    void clipSounds(std::vector<SoundFile> &soundVec);

}


#endif //HW4_SNDMIX_H
