//
// Created by Benjamin on 12/3/2015.
//

#ifndef HW4_CS229_SOUND_H
#define HW4_CS229_SOUND_H

#include <iostream>
#include <fstream>
#include <vector>

/*
 * SoundFile object
 *
 * Conatins all info and functions for a .cs229 sound
 *
 */

class SoundFile {

    public:

        // Object vars
        std::string filename;
        std::string fileType;
        int sampleRate;
        int bitRes;
        int numChannels;
        double length;
        int numSamples;
        std::vector< std::vector<int> > channelInfo;

        // Constructor
        SoundFile(std::string filename);

        // Class functions

        // Reads in a .cs229 file and stores in object variables
        bool readCS229SoundFile();

        // Writes a sound to a .cs229 file with name "filename:
        bool writeCS229SoundFile(std::string filename);

        // prints sound to stdout
        void printCSS229Sound();

    private:

        // Parses .cs229 file and palces info in fileVec
        bool parseFile(std::ifstream &inFile, std::vector<std::string> &fileVec);

        // Parses fileVec to get values for object
        bool parseVector(std::vector<std::string> &fileVec);

};

#endif //HW4_CS229_SOUND_H
