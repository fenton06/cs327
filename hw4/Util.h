//
// Created by Benjamin on 12/6/2015.
//

#ifndef HW4_UTILS_H
#define HW4_UTILS_H

#include <iostream>
#include "SoundFile.h"

/*
 * This class contains string parsing and modification functions
 */

namespace util {

    // Trims whitespace from front adn back of string
    std::string trim(std::string &str);

    // Converts string to uppercase
    std::string toUpper(std::string &str);

    //  Parses string and returns word token, delimter is any whitespace
    std::string wordToken(std::string &str);

    // Parses string and return a number as a string
    std::string numberToken(std::string &str);

}

#endif //HW4_UTILS_H

