//
// Created by Benjamin on 12/3/2015.
//

#ifndef HW4_WAVE_GENERATOR_H
#define HW4_WAVE_GENERATOR_H

#include "SoundFile.h"
#include <iostream>
#include <vector>

/*
 * This object contains all variables and functions needed to create a wave generator.
 *
 * This wave generator can create sine, pulse(square), triangle, and sawtooth waves.
 * It can also apply an ADSR volume profile, and could easily have other profiles added.
 */

class WaveGenerator {

    public:
        // Wave info
        std::string waveType; // sine, triangle, sawtooth, pulse
        double pulseUp;

        int sampleRate;
        int bitRes; // Must be 8, 16, or 32!!!
        int frequency;
        double length;
        double peakVolume; // 0 <= volume <= 1  (percentage)

        // ADSR vars
        double attack;
        double decay;
        double sustain; // 0 <= volume <= 1  (percentage)
        double release;

        // Calculated
        int numSamples;

        // Wave channel values
        std::vector< std::vector<int> > channelInfo; // No channel number needed -> creates one channel

        /*
         * Functions
         */

        // Constructor
        WaveGenerator();

        // Creates ADSR sound
        bool createADSRSound(SoundFile &waveSnd);


    private:

    // Checks to make sure all required variables have values
    bool checkWaveProps();

    // Check ADSR envelope properties
    bool checkADSRProps();

    // Quantize generated wave data
    std::vector<double> quantizeWave();

    // Create sine wave values
    std::vector<double> sampleSine();

    // Create triangle wave values
    std::vector<double> sampleTriangle();

    // Create sawtooth wave values
    std::vector<double> sampleSawtooth();

    // Create pulse(quare) wave values
    std::vector<double> samplePulse();

    //  Applies ADSR envelope volume profile
    void applyADSR(std::vector<double> &sampleVals);

    // Creates vector for channelInfo object variable
    std::vector< std::vector<int> > getChannelVals(std::vector<double> &sampleVals);

};

#endif //HW4_WAVE_GENERATOR_H
