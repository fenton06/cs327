Execuatables :

	sndcat.cpp/h:		Concatenates 2 .cs229 sound files
	sndgen.cpp/h:		Generates a waveform and can write to .cs229 file our stdout
	sndinfo.cpp/h:		Write .cs229 sound information to stdout
	sndmix.cpp/h:		Mixes sound files into one
	
Classes :

	SoundFile.cpp/h:		Conatins all info and functions for a .cs229 sound file
	WaveGenerator.cpp/h:	Contains all info and functions to write a generated wave to a .cs229 file out stdout
	Util.cpp/h:				Conatins utility functionality such as tokenizing a string