//
// Created by Benjamin on 12/3/2015.
//

#ifndef HW4_SNDGEN_H
#define HW4_SNDGEN_H


#include "WaveGenerator.h"

namespace sndgen {

    // Parses flags for generation > too many flags to leave it inline in main()
    bool parseFlags(int argc, char *argv[], WaveGenerator &waveGen);

}


#endif //HW4_SNDGEN_H
