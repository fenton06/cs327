//
// Created by Benjamin on 12/3/2015.
//

#include "sndgen.h"

using namespace std;

int main(int argc, char *argv[]) {

    string outFilename = "";

    WaveGenerator waveGen;

    // Check for switches
    for(int i = 1; i < argc; i++) {
        string arg = argv[i];
        // Display help and terminate
        if(arg.compare("-h") == 0) {

            string help = "\nUsage: sndgen [switches]\n"
                    "\n"
                    "Switches:\n"
                    "\n"
                    "\t-h\t\t\tDisplay help\n"
                    "\t-o file\t\tSpecify the output file (if omitted, writes to stdout)\n"
                    "\t\n"
                    "\tWave parameters:\n"
                    "\t\n"
                    "\t\t--sine\t\tgenerate a sine wave\n"
                    "\t\t--triangle\tgenerate a triangle wave\n"
                    "\t\t--sawtooth\tgenerate a sawtooth wave\n"
                    "\t\t--pulse\t\tgenerate a pulse wave\n"
                    "\t\t--pf p\t\tFraction of the time the pulse wave is “up”\n"
                    "\t\t\n"
                    "\t\t--bits n\tBit depth of n\n"
                    "\t\t--sr n\t\tSample rate of n\n"
                    "\t\t-f r\t\tFrequency of r Hz\n"
                    "\t\t-t r\t\tDuration of r seconds\n"
                    "\t\t-v p\t\tPeak volume (0 ≤ p ≤ 1)\n"
                    "\t\n"
                    "\tADSR Envelope Parameters:\n"
                    "\t\n"
                    "\t\t-a r\t\tAttack time of r seconds\n"
                    "\t\t-d r\t\tDecay time of r seconds\n"
                    "\t\t-s p\t\tSustain volume (0 ≤ p ≤ 1)\n"
                    "\t\t-r r\t\tRelease time of r seconds\n"
                    "\t\n"
                    "Produces a sound of a specified frequency and waveform, using a simple ADSR envelope.";

            cout << help <<endl;
            return 0;
        } else if(arg.compare("-o") == 0) {
            i++;
            arg = argv[i];
            outFilename = arg;
        }
    }

    // Parse all other flags
    if(!sndgen::parseFlags(argc, argv, waveGen)) {
        return 0;
    }

    SoundFile waveSnd(outFilename);

    if(!waveGen.createADSRSound(waveSnd)) {
        return 0;
    }

    waveSnd.channelInfo = waveGen.channelInfo;

    // No output file specified
    if(outFilename.compare("") == 0) {
        waveSnd.printCSS229Sound();
    } else {
        waveSnd.writeCS229SoundFile(outFilename);
        cout << "Sound file " << outFilename << " created successfilly!" << endl;
    }

    return 0;
}


namespace sndgen {

    bool parseFlags(int argc, char *argv[], WaveGenerator &waveGen) {

        for(int i = 1; i < argc; i++) {

            string arg = argv[i];

            if(arg.compare("--sine") == 0) {

                waveGen.waveType = "sine";

            } else if(arg.compare("--triangle") == 0) {

                waveGen.waveType = "triangle";

            } else if(arg.compare("--sawtooth") == 0) {

                waveGen.waveType = "sawtooth";

            } else if(arg.compare("--pulse") == 0) {

                waveGen.waveType = "pulse";

            } else if(arg.compare("--pf") == 0) {
                i++;

                arg = argv[i];

                try {

                    waveGen.pulseUp = stod(arg);

                    // Check if valid sample rate
                    if(waveGen.pulseUp <= 0) {
                        cerr << "Invalid pulse uptime specified!" << endl;
                        return false;
                    }

                } catch(...) {
                    cerr << "Invalid pulse uptime specified!" << endl;
                    return false;
                }

            } else if(arg.compare("--bits") == 0) {
                i++;

                arg = argv[i];

                // Check for double
                if(arg.find('.') != -1) {
                    cerr << "BitRes value invalid!!! (double)" << endl;
                    return false;
                }

                try {
                    waveGen.bitRes = stoi(arg);

                    // Check if bit resolution (bit depth)
                    if(waveGen.bitRes == 0) {
                        cerr << "Bit resolution not specified!" << endl;
                        return false;
                    } else if(waveGen.bitRes != 8 && waveGen.bitRes != 16 && waveGen.bitRes != 32) {
                        cerr << "Invalid bit resolution specified!" << endl;
                        return false;
                    }

                } catch (...){
                    cerr << "Invalid bit resolution specified!" << endl;
                    return false;
                }

            } else if(arg.compare("--sr") == 0) {
                i++;

                arg = argv[i];

                // Check for double
                if(arg.find('.') != -1) {
                    cerr << "Sample rate value invalid!!! (double)" << endl;
                    return false;
                }

                try {

                    waveGen.sampleRate = stoi(arg);

                    // Check if valid sample rate
                    if(waveGen.sampleRate <= 0) {
                        cerr << "Invalid sample rate specified!" << endl;
                        return false;
                    }

                } catch(...) {
                    cerr << "Invalid sample rate specified!" << endl;
                    return false;
                }

            } else if(arg.compare("-f") == 0) {
                i++;

                arg = argv[i];

                // Check for double
                if(arg.find('.') != -1) {
                    cerr << "Invalid frequency specified! (double)" << endl;
                    return false;
                }

                try {

                    waveGen.frequency = stoi(arg);

                    // Check if valid frequency
                    if(waveGen.frequency <= 0) {
                        cerr << "Invalid frequency specified!" << endl;
                        return false;
                    }

                } catch(...) {
                    cerr << "Invalid frequency specified!" << endl;
                    return false;
                }

            } else if(arg.compare("-t") == 0) {
                i++;

                arg = argv[i];

                try {

                    waveGen.length = stod(arg);

                    // Check if valid length
                    if(waveGen.length <= 0) {
                        cerr << "Invalid duration specified!" << endl;
                        return false;
                    }

                } catch(...) {
                    cerr << "Invalid duration specified!" << endl;
                    return false;
                }
            } else if(arg.compare("-v") == 0) {
                i++;

                arg = argv[i];

                try {

                    waveGen.peakVolume = stod(arg);

                    // Check if valid volume
                    if(waveGen.peakVolume < 0 || waveGen.peakVolume > 1) {
                        cerr << "Invalid peak volume specified!" << endl;
                        return false;
                    }

                } catch(...) {
                    cerr << "Invalid peak volume specified!" << endl;
                    return false;
                }
            } else if(arg.compare("-a") == 0) {
                i++;

                arg = argv[i];

                try {

                    waveGen.attack = stod(arg);

                    // Check if valid length
                    if(waveGen.attack <= 0) {
                        cerr << "Invalid ADSR attack duration specified!" << endl;
                        return false;
                    }

                } catch (...) {
                    cerr << "Invalid ADSR attack duration specified!" << endl;
                    return false;
                }
            } else if(arg.compare("-d") == 0) {
                i++;

                arg = argv[i];

                try {

                    waveGen.decay = stod(arg);

                    // Check if valid length
                    if(waveGen.decay <= 0) {
                        cerr << "Invalid ADSR decay duration specified!" << endl;
                        return false;
                    }

                } catch(...) {
                    cerr << "Invalid ADSR decay duration specified!" << endl;
                    return false;
                }
            } else if(arg.compare("-s") == 0) {
                i++;

                arg = argv[i];

                try {

                    waveGen.sustain = stod(arg);

                    // Check if valid sustain volume
                    if(waveGen.sustain < 0 || waveGen.sustain > 1) {
                        cerr << "Invalid ADSR sustain volume specified!" << endl;
                        return false;
                    }

                } catch(...) {
                    cerr << "Invalid ADSR sustain volume specified!" << endl;
                    return false;
                }
            } else if(arg.compare("-r") == 0) {
                i++;

                arg = argv[i];

                try {

                    waveGen.release = stod(arg);

                    // Check if valid length
                    if(waveGen.release <= 0) {
                        cerr << "Invalid ADSR release duration specified!" << endl;
                        return false;
                    }

                } catch(...) {
                    cerr << "Invalid ADSR release duration specified!" << endl;
                    return false;
                }
            } else if(arg.compare("-o") == 0) {  // Already handled!!!
                i++;
            } else {
                cerr << "Invalid switch: " << arg << endl;
                return false;
            }
        }

        // Set number of samples
        waveGen.numSamples = (int) waveGen.length * waveGen.sampleRate;

        return true;

    }

}
