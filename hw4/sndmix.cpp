//
// Created by Benjamin on 12/3/2015.
//

#include "sndmix.h"
//#include "SoundFile.h"
//#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {

    // No args passed, parse filenames from stdin
    if(argc == 1) {
        cerr << "No input filenames specified!!!" << endl;
        return 0;
    }

    string outFilename = "";

    // Check for switches
    for(int i = 1; i < argc; i++) {
        string arg = argv[i];

        // Display help and terminate
        if(arg.compare("-h") == 0) {

            string help = "\nUsage: sndmix [switches] mult1 file1 ...multn filen\n"
                    "\n"
                    "Switches:\n"
                    "\n"
                    "\t-h\t\tSpecify the output file (if omitted, writes to stdout)\n"
                    "\t-o\t\tSpecify output file\n"
                    "\t\n"
                    "This program reads all sound files passed as arguments, and “mixes” them into a single sound file.  The sample data of filei is multiplied by multi (a real value between -10 and 10), then\n"
                    "scaled data is summed to produce the output file.";

            cout << help <<endl;

            return 0;

        } else if(arg.compare("-o") == 0) {
            i++;
            outFilename = argv[i];
        }
    }

    int numFileArgs = 0;

    // Set number of files passed
    if(outFilename.compare("") == 0) {
        numFileArgs = argc - 1;
    } else {
        numFileArgs = argc - 3;
    }

    //Check if number of multn and filen is even (won't catch 2 filenames with no multn but initial error check...)
    if(numFileArgs%2 != 0) {
        cerr << "Cannot have odd number of file and mix arguments!!!" << endl;
        return 0;
    }

    // Split multn's and filenames into separate vectors...
    vector<string> multArr;
    vector<string> filenameArr;

    for(int i = argc - numFileArgs; i < argc; i++) {
        multArr.push_back(argv[i]);
        i++;
        filenameArr.push_back(argv[i]);
    }

    // Parse mults
    vector<double> multVec;

    // Check that strings in multArr start with mult and then remove mult and push to mult vector
    for(unsigned i = 0; i < multArr.size(); i++) {

        string multStr = multArr.at(i);

        if(multStr.find("mult") == 0) {

            // Erase mult from string
            multStr.erase(0,4);

            try {
                //Parse double from remaining string
                double multNum = stod(multStr);

                // Check if number is in range -10 to 10
                if(multNum < -10 || multNum > 10) {
                    cerr << "Multiplication factor in pair " << i + 1 << " not in range!!!" << endl;
                    cout << "Files not mixed." << endl;
                    return 0;
                } else {
                    multVec.push_back(multNum);
                }
            } catch(...) {
                cerr << "Multiplication factor in pair " << i + 1 << " invalid!!!" << endl;
                cout << "Files not mixed." << endl;
                return 0;
            }

        } else {
            cerr << "Invalid mult argument in pair " << i + 1 << " !!!" << endl;
            cout << "Files not mixed." << endl;
            return 0;
        }
    }

    // Create vector of sound files and input filename string
    vector<SoundFile> soundVec;
    string inFilename;

    // Parse passed filenames
    for(unsigned i = 0; i < filenameArr.size(); i++) {

        inFilename = filenameArr.at(i);
        SoundFile sound(inFilename);

        // Check if SoundFile was successfully read -> push to vector
        if(sound.readCS229SoundFile()) {
            soundVec.push_back(sound);
        } else {
            cerr << "Invalid CS229 file: " << inFilename << endl;
            return 0;
        }
    }

    // Check properties are even compatible before multiplying channel values (and check that vectors are same length)
    if(multVec.size() == soundVec.size() && sndmix::checkMixSoundProps(soundVec)) {

        // Lets multiply channel values!
        if(!(sndmix::multSounds(multVec, soundVec))) {
            cout << "Files not mixed." << endl;
            return 0;
        }

    } else {
        cerr << "File properties not compatible!!!" << endl;
        cout << "Files not mixed." << endl;
        return 0;
    }

    // Mix sounds
    sndmix::mixSounds(soundVec);

    // Clip sounds
    sndmix::clipSounds(soundVec);

    // No output file specified
    if(outFilename.compare("") == 0) {
        (soundVec.at(0)).printCSS229Sound();
    } else {
        (soundVec.at(0)).writeCS229SoundFile(outFilename);
        cout << "Sound files mixed successfilly!" << endl;
    }

    return 0;
}

namespace sndmix {

    bool checkMixSoundProps(vector<SoundFile> soundVec) {

        // Set initial param vals to first SoundFile vals to check for mix compatibility
        int mixNumChannels = soundVec.at(0).numChannels;
        int mixNumSamples = soundVec.at(0).numSamples;
        int mixSampleRate = soundVec.at(0).sampleRate;
        int mixBitRes = soundVec.at(0).bitRes;

        for(unsigned i = 1; i < soundVec.size(); i++) {
            // Check if values all match!!!
            if(soundVec.at(i).numChannels != mixNumChannels) {
                std::cerr << "Channels in sound file " << i << " does not match the rest!" << std::endl;
                return false;
            }

            if(soundVec.at(i).numSamples != mixNumSamples) {
                std::cerr << "Channels in sound file " << i << " does not match the rest!" << std::endl;
                return false;
            }

            if(soundVec.at(i).sampleRate != mixSampleRate) {
                std::cerr << "SampleRate in sound file " << i << " does not match the rest!" << std::endl;
                return false;
            }

            if(soundVec.at(i).bitRes != mixBitRes) {
                std::cerr << "BitRes in sound file " << i << " does not match the rest!" << std::endl;
                return false;
            }
        }

        return true;
    }

    bool multSounds(vector<double> &multVec, vector<SoundFile> &soundVec) {

        // Multiply channel info in each sound by corresponding multiplier and check if in range
        for(unsigned i = 0; i < multVec.size(); i++) {

            // Multiply channel info rounded according to in division...
            for(unsigned j = 0; j < soundVec.at(i).numSamples; j++) {
                for(unsigned k = 0; k < soundVec.at(i).numChannels; k++) {
                    int sampleVal = (int) (multVec.at(i) * soundVec.at(i).channelInfo[j][k]);



                    soundVec.at(i).channelInfo.at(j).at(k) = sampleVal;
                }
            }
        }

        return true;
    }

    void mixSounds(std::vector<SoundFile> &soundVec) {

        for(unsigned i = 1; i < soundVec.size(); i++) {
            // Push channel info to first sound channelInfo vector
            for(unsigned j = 0; j < soundVec.at(i).numSamples; j++) {
                for(unsigned k = 0; k < soundVec.at(i).numChannels; k++) {

                    // If adding values directly use this
                    soundVec.at(0).channelInfo.at(j).at(k) += soundVec.at(i).channelInfo.at(j).at(k);

                    // If appending channels use following...
                    //soundVec.at(0).channelInfo.at(j).push_back(soundVec.at(i).channelInfo.at(j).at(k));

                }
            }

            //soundVec.at(0).numChannels += soundVec.at(i).numChannels;
        }

        return;
    }

    void clipSounds(vector<SoundFile> &soundVec) {

        for(unsigned i = 0; i < soundVec.at(0).numSamples; i++) {
            for(unsigned j = 0; j < soundVec.at(0).numChannels; j++) {

                int val = soundVec.at(0).channelInfo.at(i).at(j);

                // Clip values if necessary
                switch(soundVec.at(0).bitRes) {

                    case 8  :
                        if(val < -127) {
                            val = -127;
                        } else if(val > 127) {
                            val = 127;
                        }
                    case 16 :
                        if(val < -327677) {
                            val = -32767;
                        } else if(val > 32767) {
                            val = 32767;
                        }
                    case 32 :
                        if(val < -2147483647) {
                            val = -2147483647;
                        } else if(val > 2147483647) {
                            val = 2147483647;
                        }

                        // No default case handling needed since bit resolution values already checked when parsing SoundFiles!
                }

                if(soundVec.at(0).channelInfo.at(i).at(j) != val) {
                    soundVec.at(0).channelInfo.at(i).at(j) = val;
                }

            }
        }
    }

}