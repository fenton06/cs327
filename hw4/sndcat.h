//
// Created by Benjamin on 12/3/2015.
//

#ifndef HW4_SNDCAT_H
#define HW4_SNDCAT_H

#include "SoundFile.h"

namespace sndcat {

    /*
     * Checks properties of eash sound file to make sure they are compatible for concatenation.
     *
     * For simplicity, numChannels, sampleRate, and bitRes must be the same.
     *
     */
    bool checkConcatSoundProps(std::vector<SoundFile> soundVec);

     // Actually concatenates the files, appends sata to SoundFile at beginning of soundVec
    void concatSounds(std::vector<SoundFile> &soundVec);

}

#endif //HW4_SNDCAT_H
