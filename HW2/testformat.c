
#include <stdio.h>
//#include <stdlib.h>

#include "weatherio.h"

int main(int argc, char **argv) {

	int validity = validate_format(argv[1]);

//    // Testing code
//    char *file = "C:\\Users\\Stacy\\Desktop\\Benjamin_Fenton_HW2\\test_files\\header_invalid1.csv";
//    int validity = validate_format(file);

    if(validity == 0) {
        printf("valid");
    } else {
        printf("not valid (line %d)\n", validity);
    }

    return 0;
}
