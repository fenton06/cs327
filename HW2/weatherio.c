#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>


#include "weatherio.h"

//Prototypes
char *trimSpace(char *str);
void freeTypes(void **types);
void freeHeaders(char **headers);

/* Checks if the file name parameter contains a validly formatted weather data
 * file specified in the first parameter. The function returns 0 if correct,
 * or the first line number that an error occurred. */
int validate_format(char *filename) {

    // Declare and allocate header array
    char **headers;
    headers = (char **) calloc((size_t) MAX_HEADERS + 1, sizeof(char*));

    // Get number of headers
    int numHeaders = read_header(filename, headers);

    // Check for invalid file or 0 headers
    if(numHeaders == -1) { //Invalid filename
        return -1;
    } else if(numHeaders == 0) {
        return 1; // Error on line 1, no headers
    }

    // Read header titles into headers
    read_header(filename, headers);

    // Open file - no check needed since read_headers already checked
    FILE *file = fopen(filename, "r");

    // Row data and line number
    int lineNum = 0;

    // Get header string and free (not needed!)
    char *headerStr = (char *) calloc((size_t) MAX_LINE_LENGTH, sizeof(char));
    fgets(headerStr, MAX_LINE_LENGTH, file);
    free(headerStr);
    lineNum++;

    int error = 0;

    // Read file until the end
    while(error == 0) {

        // Declare and allocate array of types
        void **types;
        types = (void **) calloc((size_t) numHeaders + 1, sizeof(void *)); // +1 length for null terminator

        error = read_row(file, headers, types);
        lineNum++;

        if(error > 0) { break; }  // Error in line, return line num

    }

    // Close file
    fclose(file);

    // Free headers
    freeHeaders(headers);

    if(error > 0) {         // Return line error occurrent on
        return lineNum;
    } else {                // Valid file
        return 0;
    }
}

// Returns number of headers in file
int header_columns(char *filename) {

    // Open file
    FILE *file = fopen(filename, "r");

    //check for valid filename
    if(file == NULL) {
        printf("Could not open file!!!");
        return -1;
    }

    int numHeaders = 0;
    int x = 0;

    // Loop through first line
    while((x = fgetc(file)) != 10) {
        if(x == EOF) {
            break;
        } else if(x == 44) {  // 44 is int value of a comma
            numHeaders++;
        }
    }

    // Close file
    fclose(file);

    if(x == EOF) {
        return 0;  // Empty file!
    } else {
        return numHeaders + 1;  // Increment number of headers by 1, last header has no comma after!
    }
}

// Returns number of data columns and also the C-string headers in uppercase
int read_header(char *filename, char **headers) {

    // Get number of headers
    int numHeaders = header_columns(filename);

    if(numHeaders == -1) { // Invalid filename
        return -1;
    } else if(numHeaders == 0) { // No headers
        return 0;
    }

    // Open file - no need to check for valid filename, header_columns already checked
    FILE *file = fopen(filename, "r");

    // Allocate string pointers
    int i = 0;
    for(i = 0; i < numHeaders; i++) {
        headers[i] = (char*) calloc((size_t) MAX_HEADER_LENGTH, sizeof(char));
    }

    // Null nerminator at end of header array
    headers[i] = NULL;

    char *headerRow;
    headerRow = (char *) calloc((size_t) MAX_HEADER_LENGTH, sizeof(char));

    fgets(headerRow, MAX_LINE_LENGTH, file);

    char *title = (char *) calloc((size_t) MAX_LINE_LENGTH, sizeof(char));
    strcpy(title, strtok(headerRow, ","));

    i = 0;
    while(headers[i] != NULL) {

        // Convert header to uppercase
        int j = 0;
        while(title[j])  {
            if(title[j] == 10) { title[j] = 0; }  // newline -> make NULL
            title[j] = (char) toupper(title[j]);
            j++;
        }

        strcpy(headers[i], title);
        title = strtok(NULL, ",");
        i++;
    }

    free(headerRow);
    free(title);

    // Close file
    fclose(file);
    return numHeaders;
}

// Reads a single line of data into types array, returns 0 if successful
int read_row(FILE *file, char **headers, void **types) {

    // Declare, allocate, and fill row
    char *row = (char *) calloc((size_t) MAX_LINE_LENGTH, sizeof(char));

    //END OF FILE
    if(!fgets(row, MAX_LINE_LENGTH, file)) {
        return -2;
    }

    // Get first column
    char *info;
    info = strtok(row, ",");

    int i = 0;
    while(headers[i] != NULL) {

        if(strcmp(headers[i], "IDENTIFICATION NAME") == 0) {     // NAME

            char *ID = calloc((size_t) MAX_HEADER_LENGTH, sizeof(char));

            strcpy(ID, info);

            ID = trimSpace(ID);

            types[i] = calloc((size_t) 1, sizeof(char));
            types[i] = ID;

        } else if (strcmp(headers[i], "USAF") == 0) {             // USAF

            types[i] = calloc((size_t) 1, sizeof(int));

            if(atoi(info) != 0) {
                int num = atoi(info);

                //Error check -> must be 6 digits
                if(num < 100000 || num > 999999) {
                    return 2;
                }

                *((int *) types[i]) = num;
            } else {
                return 2;
            }

        } else if (strcmp(headers[i], "NCDC") == 0) {             // NCDC

            types[i] = calloc((size_t) 1, sizeof(int));

            if(atoi(info) != 0) {
                int num = atoi(info);

                //Error check -> must be 5 digits
                if(num < 10000 || num > 99999) {
                    return 2;
                }

                *((int *) types[i]) = num;
            } else {
                return 2;
            }

        } else if (strcmp(headers[i], "DATE") == 0) {             // DATE

            types[i] = calloc((size_t) 1, sizeof(int));

            if(atoi(info) != 0) {
                int num = atoi(info);

                //Error check -> must be 8 digits
                if(num < 10000000 || num > 99999999) {
                    return 2;
                }

                *((int *) types[i]) = num;
            } else {
                return 2;
            }

        } else if (strcmp(headers[i], "HRMN") == 0) {             // HRMN

            types[i] = calloc((size_t) 1, sizeof(int));

            if(atoi(info) != 0) {
                int num = atoi(info);

                //Error check
                if(num < 0 || num > 2359) {
                    return 2;
                }

                *((int *) types[i]) = num;
            } else {
                return 2;
            }

        } else if (strcmp(headers[i], "I") == 0) {                // IDENTIFICATION (I)

            char *I = calloc((size_t) MAX_HEADER_LENGTH, sizeof(char));
            strcpy(I, info);

            I = trimSpace(I);

            // Error check -> must be between 1 and 9 or A and N
            if(!(((int) I[0] > 48 && (int) I[0] < 58) || ((int) I[0] > 64 && (int) I[0] < 79))) {
                return 2;
            }

            types[i] = calloc((size_t) 1, sizeof(char));
            types[i] = I;

        } else if (strcmp(headers[i], "TYPE") == 0) {             // TYPE

            char *TYPE = calloc((size_t) MAX_HEADER_LENGTH, sizeof(char));
            strcpy(TYPE, info);

            TYPE = trimSpace(TYPE);

            if(!(strcmp(TYPE, "99999") == 0 ||
                    strcmp(TYPE, "AERO") == 0 ||
                    strcmp(TYPE, "AUST") == 0 ||
                    strcmp(TYPE, "AUTO") == 0 ||
                    strcmp(TYPE, "BOGUS") == 0 ||
                    strcmp(TYPE, "BRAZ") == 0 ||
                    strcmp(TYPE, "COOPD") == 0 ||
                    strcmp(TYPE, "COOPS") == 0 ||
                    strcmp(TYPE, "CRB") == 0 ||
                    strcmp(TYPE, "CRN05") == 0 ||
                    strcmp(TYPE, "CRN15") == 0 ||
                    strcmp(TYPE, "FM-12") == 0 ||
                    strcmp(TYPE, "FM-13") == 0 ||
                    strcmp(TYPE, "FM-14") == 0 ||
                    strcmp(TYPE, "FM-15") == 0 ||
                    strcmp(TYPE, "FM-16") == 0 ||
                    strcmp(TYPE, "FM-18") == 0 ||
                    strcmp(TYPE, "GREEN") == 0 ||
                    strcmp(TYPE, "MESOS") == 0 ||
                    strcmp(TYPE, "MEXIC") == 0 ||
                    strcmp(TYPE, "NSRDB") == 0 ||
                    strcmp(TYPE, "PCP15") == 0 ||
                    strcmp(TYPE, "PCP60") == 0 ||
                    strcmp(TYPE, "S-S-A") == 0 ||
                    strcmp(TYPE, "SA-AU") == 0 ||
                    strcmp(TYPE, "SAO") == 0 ||
                    strcmp(TYPE, "SAOSP") == 0 ||
                    strcmp(TYPE, "SHEF") == 0 ||
                    strcmp(TYPE, "SMARS") == 0 ||
                    strcmp(TYPE, "SOD") == 0 ||
                    strcmp(TYPE, "SOM") == 0 ||
                    strcmp(TYPE, "SURF") == 0 ||
                    strcmp(TYPE, "SY-AE") == 0 ||
                    strcmp(TYPE, "SY-AU") == 0 ||
                    strcmp(TYPE, "SY-MT") == 0 ||
                    strcmp(TYPE, "SY-SA") == 0 ||
                    strcmp(TYPE, "WBO") == 0 ||
                    strcmp(TYPE, "WNO") == 0)){
                return 2;
            };

            types[i] = calloc((size_t) 1, sizeof(char));
            types[i] = TYPE;

        } else if (strcmp(headers[i], "QCP") == 0) {              // QCP

            char *QCP = calloc((size_t) MAX_HEADER_LENGTH, sizeof(char));
            strcpy(QCP, info);

            QCP = trimSpace(QCP);

            if(strlen(QCP) != 4) {
                return 2;
            }

            types[i] = calloc((size_t) 1, sizeof(char));
            types[i] = QCP;

        } else if (strcmp(headers[i], "WIND DIR") == 0) {         // WIND DIR

            types[i] = calloc((size_t) 1, sizeof(int));

            if(atoi(info) != 0) {
                int num = atoi(info);

                // Error check -> must be between 0 and 360, or 999 as default
                if(!((num >= 0 && num <= 360) || num == 999)) {
                    return 2;
                }

                *((int *) types[i]) = num;
            } else {
                return 2;
            }

        } else if (strcmp(headers[i], "WIND DIR Q") == 0) {       // WIND DIR Q

            types[i] = calloc((size_t) 1, sizeof(int));

            if(atoi(info) != 0) {
                int num = atoi(info);

                // Error check -> must between 0 and 9
                if(num < 0 || num > 9) {
                    return 2;
                }

                *((int *) types[i]) = num;
            } else {
                return 2;
            }

        } else if (strcmp(headers[i], "WIND DIR I") == 0) {       // WIND DIR I

            char *windDirI = calloc((size_t) MAX_HEADER_LENGTH, sizeof(char));
            strcpy(windDirI, info);

            windDirI = trimSpace(windDirI);

            if(!((int) windDirI[0] == 65 ||  // A
                    (int) windDirI[0] == 66 ||  // B
                    (int) windDirI[0] == 67 ||  // C
                    (int) windDirI[0] == 72 ||  // H
                    (int) windDirI[0] == 78 ||  // N
                    (int) windDirI[0] == 81 ||  // Q
                    (int) windDirI[0] == 82 ||  // R
                    (int) windDirI[0] == 84 ||  // T
                    (int) windDirI[0] == 86)) {  // V
                return 2;
            }

            types[i] = calloc((size_t) 1, sizeof(char));
            types[i] = windDirI;

        } else if (strcmp(headers[i], "WIND SPD") == 0) {         // WIND SPEED

            types[i] = calloc((size_t) 1, sizeof(double));

            if(atof(info) != 0) {
                double num = atoi(info);

                // Error check -> must be 4 digits
                if(num < 0 || num > 9999) {
                    return 2;
                }

                // Account for scale of 10!
                num  = num * 10;

                *((double *) types[i]) = num;
            } else {
                return 2;
            }

        } else if (strcmp(headers[i], "WIND SPD Q") == 0) {       // WIND SPEED Q

            types[i] = calloc((size_t) 1, sizeof(int));

            if(atoi(info) != 0) {
                int num = atoi(info);

                // Error check -> must between 0 and 9
                if(num < 0 || num > 9) {
                    return 2;
                }

                *((int *) types[i]) = num;
            } else {
                return 2;
            }

        } else if (strcmp(headers[i], "TEMP") == 0) {             // TEMP

            types[i] = calloc((size_t) 1, sizeof(double));

            if(atof(info) != 0) {
                double num = atof(info);

                // Error check -> must be 5 length of 5, 4 digits with indicator!
                if(num < -9999 || num > 9999) {
                    return 2;
                }

                // Take care of scale!
                num = num * 10;

                *((double *) types[i]) = num;
            } else {
                return 2;
            }

        } else if (strcmp(headers[i], "TEMP Q") == 0) {           // TEMP Q

            char *tempQ = calloc((size_t) MAX_HEADER_LENGTH, sizeof(char));
            strcpy(tempQ, info);

            tempQ = trimSpace(tempQ);

            if(!(((int) tempQ[0] > 47 || (int) tempQ[0] < 58) ||  // Between 0 and 9
                    (int) tempQ[0] == 65 ||  // A
                    (int) tempQ[0] == 67 ||  // C
                    (int) tempQ[0] == 73 ||  // I
                    (int) tempQ[0] == 77 ||  // M
                    (int) tempQ[0] == 80 ||  // P
                    (int) tempQ[0] == 82 ||  // R
                    (int) tempQ[0] == 85)) {  // U
                return 2;
            }

            types[i] = calloc((size_t) 1, sizeof(char));
            types[i] = tempQ;

        } else if (strcmp(headers[i], "DEWPT") == 0) {            // DEWPT

            types[i] = calloc((size_t) 1, sizeof(double));

            if(atof(info) != 0) {
                double num = atof(info);

                // Error check -> must be 4 digits
                if(num < -9999 || num > 9999) {
                    return 2;
                }

                // Account for scale of 10!
                num  = num * 10;

                *((double *) types[i]) = num;
            } else {
                return 2;
            }

        } else if (strcmp(headers[i], "DEWPT Q") == 0) {          // DEWPT Q: 1 char string + 1 null

            char *dewQ = calloc((size_t) MAX_HEADER_LENGTH, sizeof(char));
            strcpy(dewQ, info);

            dewQ = trimSpace(dewQ);

            if(!(((int) dewQ[0] > 47 && (int) dewQ[0] < 58) &&  // Between 0 and 9
               (int) dewQ[0] == 65 ||  // A
               (int) dewQ[0] == 65 ||  // C
               (int) dewQ[0] == 65 ||  // I
               (int) dewQ[0] == 65 ||  // M
               (int) dewQ[0] == 65 ||  // P
               (int) dewQ[0] == 65 ||  // R
               (int) dewQ[0] == 65)) {  // U
                return 2;
            }

            types[i] = calloc((size_t) 1, sizeof(char));
            types[i] = dewQ;

        } else if (strcmp(headers[i], "SLP") == 0) {              // SLP

            types[i] = calloc((size_t) 1, sizeof(double));

            if(atof(info) != 0) {
                double num = atof(info);

                // Error check -> must be 5 digits
                if(num < 0 || num > 9999) {
                    return 2;
                }

                *((double *) types[i]) = num;
            } else {
                return 2;
            }

        }  else if (strcmp(headers[i], "SLP Q") == 0) {           // SLP Q

            types[i] = calloc((size_t) 1, sizeof(int));

            if(atoi(info) != 0) {
                int num = atoi(info);

                // Error check -> must between 0 and 9
                if(num < 0 || num > 9) {
                    return 2;
                }

                *((int *) types[i]) = num;
            } else {
                return 2;
            }

        } else if (strcmp(headers[i], "RHX") == 0) {              // RHX

            types[i] = calloc((size_t) 1, sizeof(int));

            if(atoi(info) != 0) {
                int num = atoi(info);

                // Error check -> must have a max length of 3
                if(num < 0 || num > 100) {
                    return 2;
                }

                *((int *) types[i]) = num;
            } else {
                return 2;
            }
        } else {
            return 3;  // Invalid header name
        }

        // Get next column
        info = strtok(NULL, ",");

        i++;

        // Check if there is more or less data than headers
        if(headers[i] == NULL && info != NULL) {
            return 4;
        }
    }



    // Free row and types after use
    free(row);
    freeTypes(types);

    return 0;
}

// Trim space from front and back of string
char *trimSpace(char *str) {

    char *end;

    // Trim leading space(s)
    while(isspace(*str)){
        str++;
    }

    if(*str == 0) {  // All spaces
        return str;
    }

    // Trim trailing space(s)
    end = str + strlen(str) - 1;

    while(end > str && isspace(*end)) {
        end--;
    }

    // Write new null terminator
    *(end + 1) = 0;

    return str;

}

// Free allocated memory in types
void freeTypes(void **types) {

    int i = 0;

    while(types[i] != NULL) {
        free(types[i]);
        i++;
    }

    free(types[i]); // Free null terminator

    free(types);
}

// Free allocated memory for headers
void freeHeaders(char **headers) {

    int i = 0;

    // Free headers
    for(i = 0; i < MAX_HEADERS + 1; i++) { // +1 to take care of null terminator
        free(headers[i]);
    }

    free(headers);
}
