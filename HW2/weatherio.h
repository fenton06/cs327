/*
 * weatherio.h
 *
 *  Created on: Sep 22, 2015
 *      Author: Benjamin Fenton
 */

#ifndef WEATHERIO_H_
#define WEATHERIO_H_

// Max length of 64 chars for header label, including null terminator
#define MAX_HEADERS 20
#define MAX_HEADER_LENGTH 64
#define MAX_LINE_LENGTH 256

/*
 * Checks if the file name parameter contains a validly formatted weather
 * data file specified in the first parameter. The function returns 0 if correct, or the first line number that an error occurred.
 *
 * Return 0 = Valid file
 * Return x = x is line number where error occurs
 */

int validate_format(char *);

/*
 * This function returns the number of data columns (header columns) in the file name specified in
 * the first parameter passed to main
 */

int header_columns(char *);

/*
 * This function returns the number of data columns in the file, and in addition, returns the c-strings
 * for those headers in an array that is passed as the second parameter.
 */

int read_header(char *, char **);


/*
 * This functions returns a row of data in an open file and assumes that the file pointer is positioned
 * at a row past the header file. The first parameter is a FILE * type to an open file. The second
 * parameter is the array of c-strings that are the headers, as returned by the read_header function.
 * the last parameter is an array of pointers where each pointer points to the appropriate type for
 * the item read in the column as defined by the header information. Function returns 0 if the row was
 * read correctly, and otherwise returns an integer that indicates the type of error.
 *
 * Return -2:  End of file - Not an error!
 * Return -1: invalid file name
 * Return 2: Invalid data format
 * Return 3: Invalid header
 * Return 4: Not enough row data
 */

int read_row(FILE *, char **, void **);

#endif /* WEATHERIO_H_ */
