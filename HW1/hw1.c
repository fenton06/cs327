#include <stdio.h>
#include <stdlib.h>

int main() {
	printf("Please enter a number greater than 2: \n");

	int n = 2;

	// Receive input
	scanf("%d", &n);

	//Increment n so that last array place has index of n
	n++;

	// Create an array with length of n + 1
	int *nums;
	nums = malloc(n * sizeof(int));

	int i = 0;

	// Set all values to 1 (meaning prime)
	for (i; i < n; i++) {
		nums[i] = 1;
	}

	i = 2;

	// Find prime numbers using Sieve of Erasthones algorithm
	for (i; i < n; i++) {
		// Check if number is prime
		if (nums[i] == 1) {
			printf("%d\n", i);
			int j = i + i;
			// Set all multiples of prime number to 0
			while (j < n) {
				nums[j] = 0;
				j = j + i;
			}
		}
	}

	return 0;
}
